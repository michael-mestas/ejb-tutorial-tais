/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ejb;

import javax.ejb.Stateless;

/**
 *
 * @author michael
 */
@Stateless(mappedName="ejb/StartedSessionBeanRemote")
public class MySession implements MySessionRemote {

    @Override
    public String getResult() {
        
        return "Este el session Bean de michael";
    }

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    
}
